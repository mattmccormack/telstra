from setuptools import setup

setup(
    name="Telstra",
    version="0.2",
    author="Matthew McCormack",
    author_email="matt@mccormack.email",
    packages=["telstrasms", ],
    install_requires=[
        "oauthlib",
        "requests_oauthlib"
    ]
)
