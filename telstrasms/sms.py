from oauthlib.oauth2 import BackendApplicationClient, TokenExpiredError
from requests_oauthlib import OAuth2Session


class TelstraSMS(object):
    """
    Token expires in one hour
    telstra doesn't allow token refreshes so need to catch TokenExpiredError and re-run authenticate()
    """
    messages_url = "https://api.telstra.com/v1/sms/messages"
    token_url = "https://api.telstra.com/v1/oauth/token"
    scope = ["SMS"]

    def __init__(self, app_key, app_secret):
        self.app_key = app_key
        self.app_secret = app_secret
        self.session = None
        self.authenticate()

    def authenticate(self):
        client = BackendApplicationClient(client_id=self.app_key)
        self.session = OAuth2Session(client=client)
        self.session.fetch_token(token_url=self.token_url,
                                 client_id=self.app_key,
                                 client_secret=self.app_secret,
                                 scope=self.scope)

    def send(self, recipient, message_text):
        # check message length, raise error if > 160 chars
        if len(message_text) > 160:
            raise ValueError("Message length exceeds 160 chars")
        # check recipient is number
        if not recipient.isdigit():
            raise ValueError("Recipient is not number")

        headers = {"Content-Type": "application/json"}
        payload = {"to": recipient, "body": message_text}

        try:
            r = self.session.post(self.messages_url, headers=headers, json=payload)
        except TokenExpiredError:
            self.authenticate()
            r = self.session.post(self.messages_url, headers=headers, json=payload)

        message_id = r.json()["messageId"]
        # return message_id
        return Message(self, message_id, message_text)

    def get_status(self, message_id):
        """
        PEND    The message is pending and has not yet been sent to the intended recipient
        SENT    The message has been sent to the intended recipient, but has not been delivered yet
        DELIVRD The message has been delivered to the intended recipient
        READ    The message has been read by intended recipient and the recipient's response has been received
        """
        url = self.messages_url + "/" + message_id
        try:
            r = self.session.get(url)
        except TokenExpiredError:
            self.authenticate()
            r = self.session.get(url)
        return r.json()

    def get_response(self, message_id):
        url = self.messages_url + "/" + message_id + "/response"
        try:
            r = self.session.get(url)
        except TokenExpiredError:
            self.authenticate()
            r = self.session.get(url)
        return r.json()


class Message(object):
    def __init__(self, api, message_id, message_text):
        self.api = api
        self.message_id = message_id
        self.message_text = message_text

    def get_status(self):
        return self.api.get_status(self.message_id)

    def get_response(self):
        return self.api.get_response(self.message_id)

    @classmethod
    def create(cls, message_text):
        pass
